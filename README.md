# failover-orchestrator

## Concept
Some designs need persistent storage in RWX. But CSI with RWX support can be resource consumming and do not provide satisfying solution. there more choices on CSI with RWO support and they are reliable 
To overcome the need of RWX, we can deploy the whole stack on single node with RWO persistent volume:

- [ ] DRP is covered by adding replicated nodes in active/passive modes
- [ ] The failover should be managed by orchestrator in HA mode via node labeling
- [ ] Orchestrators should have leader election managed by leash as demonstrated in this exmaple: https://github.com/mjasion/golang-k8s-leader-example/blob/main/main.go


## Authors and acknowledgment
NGUYEN Minh Quan

## License
Apache

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
